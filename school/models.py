from django.db import models


class School(models.Model):
    name = models.CharField(max_length=50)

    class Meta:
        verbose_name = 'School'
        verbose_name_plural = 'Schools'

    def __unicode__(self):
       return self.name


class Classroom(models.Model):
    name = models.CharField(max_length=50)
    school = models.OneToOneField(School)

    class Meta:
        verbose_name = 'Classroom'
        verbose_name_plural = 'Classrooms'

    def __unicode__(self):
       return self.name


class Student(models.Model):
    name = models.CharField(max_length=50)
    surname = models.CharField(max_length=50)
    classroom = models.OneToOneField(Classroom)

    class Meta:
        verbose_name = 'Student'
        verbose_name_plural = 'Students'

    def __unicode__(self):
       return self.name + " " + self.surname


