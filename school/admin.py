from django.contrib import admin

from school.models import Classroom, School, Student

admin.site.register(School)
admin.site.register(Classroom)
admin.site.register(Student)
